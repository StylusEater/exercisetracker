import os
import pexpect
import subprocess
from time import time 
from distutils.spawn import find_executable

class SensorTag:
    def __init__(self):
        self.error = ""
        self.hciconfig = find_executable("hciconfig")
        if not self.hciconfig:
            self.error = "ERROR: hciconfig must be installed.\n"
        self.hcitool = find_executable("hcitool")
        if not self.hcitool:
            self.error = "ERROR: hcitool must be installed.\n"
        self.gatttool = find_executable("gatttool")
        if not self.gatttool:
            self.error = "ERROR: gatttool must be installed.\n"
        self.tags = dict()
        self.SENSORS = os.getcwd() + "/sensors/SENSORS"
        self.log = ""

    def get_error(self):
        return self.error    

    def detect(self):
        pass
    
    def connect(self):
        try:
            sensors = open(self.SENSORS, 'r')
            connection = ""
            timestamp = str(time())[0:-3]
            log_file = os.getcwd() + '/log/' + timestamp + '.run' 
            log = open(log_file,"w+")
            self.log = log
            log.write("STARTING RUN @" + timestamp)
            subprocess.call([self.hciconfig,"hci0","reset"]) 
            for sensor in sensors:
                sensor = sensor.rstrip()
                log_name = os.getcwd() + "/log/" + sensor + "_" + timestamp + ".data"
                connection = pexpect.spawn('/bin/bash -c "gatttool -b ' + \
                    sensor + ' -I | tee ' + log_name + '"')
                connection.logfile = log
                connection.expect('\[LE\]>', timeout=600)
                connection.sendline('connect')
                connection.expect('Connection successful')
                self.tags[sensor] = connection
            sensors.close()
        except IOError:
            self.error = "ERROR: Static Sensor Tag file does not exist."
    
    def enable(self):
        enable_accelerometer = '31 01'
        stream_accelerometer = '2E 0100'
        enable_magnetomer = '44 01'
        stream_magnetomer = '41 0100'
        enable_gyroscope = '5B 07'
        stream_gyroscope = '58 0100'
        for tag in self.tags.values():
            tag.sendline('char-write-cmd ' + enable_accelerometer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_accelerometer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + enable_magnetomer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_magnetomer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + enable_gyroscope) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_gyroscope) 
            tag.expect('\r\n')
        start_timestamp = str(time())[0:-3]
        self.log.write("ENABLED @" + start_timestamp)

    def disable(self):
        disable_accelerometer = '31 00'
        stream_accelerometer = '2E 0000'
        disable_magnetomer = '44 00'
        stream_magnetomer = '41 0000'
        disable_gyroscope = '5B 00'
        stream_gyroscope = '58 0000'
        for tag in self.tags.values():
            tag.sendline('char-write-cmd ' + disable_accelerometer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_accelerometer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + disable_magnetomer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_magnetomer) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + disable_gyroscope) 
            tag.expect('\r\n')
            tag.sendline('char-write-cmd ' + stream_gyroscope) 
            tag.expect('\r\n')
            tag.__del__()
        stop_timestamp = str(time())[0:-3]
        self.log.write("DISABLED @" + stop_timestamp)
        subprocess.call(["sudo","pkill","tee"])

#!/usr/bin/perl

####
## sudo apt-get install libmodern-perl-perl libswitch-perl

use Cwd;
use Modern::Perl '2010';
use Switch;

#######################
## POST PROCESS DATA ##
#######################

my $ACCELEROMTER_PREFIX = "0x002d";
my $MAGNETOMER_PREFIX = "0x0040";
my $GYROSCOPE_PREFIX = "0x0057";

die "ERROR: First argument must be a file.\n" if (scalar @ARGV != 1);

my $file = $ARGV[0];
chomp($file);
my $timestamp = `date +%s`;
chomp($timestamp);
my $output = getcwd . "/$timestamp\_processed.log";
open(LOG, $file) or die "ERROR: Cannot open data log.\n$!\n";
my ($line,@partial,@parts,$accel,$accelx,$accely,$accelz,$mag,$magx,
    $magy,$magz,$gyro,$gyrox,$gyroy,$gyroz);
open(OUTPUT, ">>$output") or die "ERROR: Cannot open output log.\n$!\n";
print "Output saved to $output.\n";
while(<LOG>)
{
    $line = $_;
    chomp($line);
    if ($line =~ /Notification/)
    {
        switch ($line)
        {
            case /$ACCELEROMTER_PREFIX/
            {
                @partial = split /value: /,$line;
                $accel = $partial[1];
                chomp($accel);
                @parts = split(/ /,$accel);
                $accelx = (hex $parts[0]) / 64;
                $accely = (hex $parts[1]) / 64;
                $accelz = (hex $parts[2]) / 64; 
                print OUTPUT "A: x $accelx y $accely z $accelz \n";
            }
            case /$MAGNETOMER_PREFIX/
            {
                @partial = split /value: /,$line;
                $mag = $partial[1];
                chomp($mag);
                @parts = split(/ /,$mag);
                $magx = hex '0x' . $parts[0] . $parts[1]; 
                $magy = hex '0x' . $parts[2] . $parts[3]; 
                $magz = hex '0x' . $parts[4] . $parts[5]; 
                $magx = $magx/(65536/2000);
                $magy = $magy/(65536/2000);
                $magz = $magz/(65536/2000);
                print OUTPUT "M: x $magx y $magy z $magz\n";
            }             
            case /$GYROSCOPE_PREFIX/
            {
                @partial = split /value: /,$line;
                $gyro = $partial[1];
                chomp($gyro);
                @parts = split(/ /,$gyro);
                $gyrox = hex '0x' . $parts[0] . $parts[1];
                $gyroy = hex '0x' . $parts[2] . $parts[3];
                $gyroz = hex '0x' . $parts[4] . $parts[5];
                $gyrox = $gyrox/(65536/500);
                $gyroy = $gyroy/(65536/500);
                $gyroz = $gyroz/(65536/500);
                print OUTPUT "G: x $gyrox y $gyroy z $gyroz\n";
            }
            else 
            {
                print "ERROR: Unsupported sensor data ... \n $line \n";
            }
        } 
    }
}
close(LOG);
close(OUTPUT);

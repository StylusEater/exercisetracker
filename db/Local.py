import sqlite3

class Local:
    def __init__(self, name="db/et.db"):
        self.db = name
        self.connection = ""
        self.cursor = ""

    def init(self):
        try:
            with open(self.db):
                pass
        except IOError:
            ## Create the database
            self.connection = sqlite3.connect(self.db)
            self.cursor = self.connection.cursor()
            self.cursor.execute('''CREATE TABLE sensors
                (id INT, created TEXT, blid TEXT)''')
            self.connection.commit()
            self.connection.close()

    def open(self):
        try:
            self.connection = sqlite3.connect(self.db)
            self.cursor = self.connection.cursor()
        except IOError:
            print "ERROR: Please create the database."
            exit(-1)
    
    def close(self):
        self.connection.close()

    def get_cursor(self):
        self.open()
        return self.cursor()

    def register(self,blid):
        if not blid:
            print "ERROR: Bluetooth Device ID required."
            exit()
        cursor = self.get_cursor()
        from datetime import date
        today = date.today().strftime("%Y-%m-%d")
        cursor.execute("INSERT INTO sensors \
            VALUES (NULL,?,?)",today,blid)
        self.connection.commit()

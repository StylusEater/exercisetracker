============
DEPENDENCIES
============

DEBIAN GNU/LINUX:

    sudo apt-get install 
        python-pexpect python-sqlite libdbus-1-dev 
        libusb-1.0-0-dev libusb-dev libudev-dev 
        libical-dev build-essential libglib2.0-dev
        libreadline-dev

    mkdir -p ~/bluez/install
    cd ~/bluez
    wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.7.tar.xz
    xz -d bluez-5.7.tar.gz
    tar -xf bluez-5.7.tar
    cd bluez-5.7
    ./configure --prefix=~/bluez/install --with-systemdsystemunitdir=/lib/systemd/system
                --with-systemduserunitdir=/usr/lib/systemd
    make
    sudo make install 

    echo "export PATH=$PATH:~/bluez/install/bin:~/bluez/bluez-5.7/attrib" >> ~/.profile

    sudo ln -s /home/pi/bluez/install/bin/hcitool /usr/local/bin/hcitool
    sudo ln -s /home/pi/bluez/install/bin/hciconfig /usr/local/bin/hciconfig
    sudo ln -s /home/pi/bluez/bluez-5.7/attrib/gatttool /usr/local/bin/gatttool

    logout then log back in


============
INSTRUCTIONS
============

Download the latest version of the project and make the log directory.

    git clone git@bitbucket.org:StylusEater/exercisetracker.git
    cd exercisetracker
    mkdir log

then do the following:

    1) Plugin BLE USB adapter.
    2) sudo hcitool lescan 
    3) Add each SensorTag address to sensor/SENSORS
    4) sudo python et.py
    5) Push the side button on each sensor tag. 
    6) Select 'e' to enable data collection.
    7) Press 'q' to stop data collection.


    ## If you need to reset the BLE adpater then do the following:
    sudo /home/pi/bluez/install/bin/./hciconfig hci0 reset

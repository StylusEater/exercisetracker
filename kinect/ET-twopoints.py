from freenect import sync_get_depth as get_depth
from freenect import sync_get_video as get_video
import numpy as np
import cv,cv2
import pygame
import Image
import math

from Xlib import X,display
import Xlib.XK
import Xlib.error
import Xlib.ext.xtest

class Colors:
    def __init__(self):
        self.BLACK = (0,0,0)
        self.RED = (255,0,0)
        self.GREEN = (0,255,0)
        self.PURPLE = (255,0,255)
        self.BLUE = (0,0,255)
        self.WHITE = (255,255,255)
        self.YELLOW = (255,255,0)

class Contours:
    def __init__(self,sequence):
        self.centroid = list()
        self.hull = list()
        self.contours = list()
        self.hullArea = list()
        self.contourArea = list()
        self.counter = 0
        self.sequence = sequence

    def get(self):
        while self.sequence:
            if abs(cv.ContourArea(self.sequence)) > 2000:
                self.contourArea.append(cv.ContourArea(self.sequence))
                m = cv.Moments(self.sequence)
                try:
                    m10 = int(cv.GetSpatialMoment(m,1,0))
                    m00 = int(cv.GetSpatialMoment(m,0,0))
                    m01 = int(cv.GetSpatialMoment(m,0,1))
                    self.centroid.append((int(m10/m00), int(m01/m00)))
                    convexHull = cv.ConvexHull2(self.sequence,cv.CreateMemStorage(),return_points=True)
                    self.hullArea.append(cv.ContourArea(convexHull))
                    self.hull.append(list(convexHull))
                    self.contours.append(list(self.sequence))
                    self.counter += 1
                except: 
                    pass
            self.sequence = self.sequence.h_next()


def cacheAppendMean(cache, val):
    cache.append(val)
    del cache[0]
    return np.mean(cache)


centroids = list()
color = Colors()
pygame.init()
pygame.display.set_caption('CSU Exercise Tracker')
xSize,ySize = 640,480
screen = pygame.display.set_mode((xSize,ySize),pygame.RESIZABLE)
flippedScreen = pygame.display.set_mode((xSize,ySize),pygame.RESIZABLE)
constList = lambda length, val: [val for _ in range(length)]
chac = constList(5,12000)
arc = constList(5,100)

running = True
hand = False
count = 0
while running:
    screen.fill(color.BLACK)
    (depth,_) = get_depth()
    (img,_) = get_video()
    
    new_depth = depth.astype(np.float32)
    new_img = pygame.image.load('snapshot.bmp')

    ###
    # threshold
    #   -- applies a fixed level threshold to each array element
    #      (Input, Output, Threshold, Max, type)
    #   -- http://docs.opencv.org/modules/imgproc/doc/miscellaneous_transformations.html#threshold
    #   -- THRESH_BINARY_INV: if src(x,y) > thresh
    #   -- positioning x,y
    _,fore = cv2.threshold(new_depth, 600, 255, cv2.THRESH_BINARY_INV)
    _,back = cv2.threshold(new_depth, 900, 255, cv2.THRESH_BINARY_INV)
    
    fore_contours = cv.FindContours(cv.fromarray(fore.astype(np.uint8)),cv.CreateMemStorage(),mode=cv.CV_RETR_EXTERNAL)
    back_contours = cv.FindContours(cv.fromarray(back.astype(np.uint8)),cv.CreateMemStorage(),mode=cv.CV_RETR_EXTERNAL)

    fc = Contours(fore_contours)
    fc.get()
    bc = Contours(back_contours)
    bc.get()

    pygame.draw.circle(screen,color.WHITE,(250,200),10)
    pygame.draw.circle(screen,color.PURPLE,(250,400),10)


    ## LOGIC
    for contour in bc.contours:
        pygame.draw.lines(screen,color.WHITE,True,contour,1)
    for blob in range(fc.counter):
        #pygame.draw.circle(screen,color.GREEN,fc.centroid[blob],10)
        centroids.append(fc.centroid[blob])
        #pygame.draw.lines(screen,color.RED,True,fc.hull[blob],1)
        pygame.draw.lines(screen,color.BLUE,True,fc.contours[blob],1)
        for each_point in fc.contours[blob]:
            arm_length = math.sqrt(math.pow((250 - 250),2) + math.pow((200 - 400),2))
            if math.sqrt(math.pow((each_point[0] - 250),2) + math.pow((each_point[1] - 400),2)) == arm_length:
                print "MATCHED"

        #for x in xC:
        #    if 240 < x < 260:
        #       print "Touched #" + str(count) + " times."
        #       count += 1
        #for vertex in fc.hull[blob]:
        #    pygame.draw.circle(screen,color.PURPLE,vertex,5)

    flippedScreen = pygame.transform.flip(screen,1,0)
    screen.blit(new_img,(0,0))
    screen.blit(flippedScreen,(0,0))
    pygame.display.flip()

    
    try:
        cx = fc.centroid[0][0]
        cy = fc.centroid[0][1]
        if hand:
            dX = cx - strX
            dY = strY - cy
            if abs(dX) > 1:
                mx = mptr["root_x"] - 2*dX
            if abs(dY) > 1:
                my = mptr["root_y"] - 2*dY
            strX = cx
            strY = cy
            ca = cacheAppendMean(chac,fc.hullarea[0])
            ar = cacheAppendMean(arc,fc.countourArea[0]/ca)
        else:
            strX = cx
            strY = cy
            hand = True
    except:
        hand = False

    for event in pygame.event.get():
        if event.type is pygame.QUIT:
            running = False

